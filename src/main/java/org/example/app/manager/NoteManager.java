package org.example.app.manager;

import org.example.app.dto.NoteRQ;
import org.example.app.dto.NoteRS;
import org.example.framework.multipart.Part;

import java.util.List;
import java.util.Map;
import java.util.List;

public interface NoteManager {
    NoteRS getById(long id);

    List<NoteRS> getAll();

    NoteRS create(final Map<String, List<Part>> parts);

    NoteRS removeById(long id);

    NoteRS updateById(long id, Map<String, List<Part>> parts);
}

