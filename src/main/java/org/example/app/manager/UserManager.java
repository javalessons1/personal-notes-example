package org.example.app.manager;

import org.example.app.dto.GetUserByIdRS;
import org.example.app.dto.UserRegisterRQ;
import org.example.app.dto.UserRegisterRS;
import org.example.framework.security.auth.Authenticator;
import org.example.framework.security.auth.AuthenticationToken;

public interface UserManager extends Authenticator {

  GetUserByIdRS getById(long id);

  UserRegisterRS create(UserRegisterRQ requestDTO);

  @Override
  boolean authenticate(AuthenticationToken request);
}

