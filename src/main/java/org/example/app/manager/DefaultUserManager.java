package org.example.app.manager;

import lombok.extern.slf4j.Slf4j;
import org.example.app.repository.UserRepository;
import org.example.app.dto.GetUserByIdRS;
import org.example.app.dto.UserRegisterRQ;
import org.example.app.dto.UserRegisterRS;
import org.example.app.entity.UserEntity;
import org.example.app.exception.ItemNotFoundException;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.annotation.Audit;
import org.example.framework.security.auth.AuthenticationToken;
import org.example.framework.security.auth.LoginPasswordAuthenticationToken;
import org.example.framework.server.exception.UnsupportAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.Optional;


@Slf4j
@Component
public class DefaultUserManager implements UserManager {
  private final PasswordEncoder encoder;
  private final UserRepository repository;
  private boolean isRead = false;

  public DefaultUserManager(PasswordEncoder encoder, UserRepository rep) {
    this.encoder = encoder;
    this.repository = rep;
  }

  @Override
  @Audit
  public UserRegisterRS create(final UserRegisterRQ requestDTO) {

    final String login = requestDTO.getLogin().trim().toLowerCase();
    final String password = requestDTO.getPassword();
    final String encodedPassword = encoder.encode(password);
    UserEntity user = new UserEntity(0, login, encodedPassword, false);
    repository.save(user);
    return new UserRegisterRS(user.getLogin());
  }

  @Override
  public GetUserByIdRS getById(final long id) {
    final Optional<UserEntity> user;
    user = repository.getById(id);
    return user
            .map(o -> new GetUserByIdRS(o.getId(), o.getLogin()))
            .orElseThrow(ItemNotFoundException::new)
            ;
  }

  @Override
  public boolean authenticate(final AuthenticationToken request) {
    if (!(request instanceof LoginPasswordAuthenticationToken)) {
      throw new UnsupportAuthenticationToken();
    }
    final LoginPasswordAuthenticationToken converted = (LoginPasswordAuthenticationToken) request;
    final String login = converted.getLogin();
    final String password = (String) converted.getCredentials();
    final String encodedPassword;
    List<UserEntity> users = repository.getAll(0, 50);
    for (UserEntity user : users) {
      if (user.getLogin().equals(login) && encoder.matches(password, (user.getPassword()))) {
        return true;
      }
    }
    return false;
  }
}
