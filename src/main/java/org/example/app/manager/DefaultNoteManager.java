package org.example.app.manager;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.repository.UserRepository;
import org.example.app.dto.NoteRQ;
import org.example.app.dto.NoteRS;
import org.example.app.entity.NoteEntity;
import org.example.app.entity.UserEntity;
import org.example.app.exception.ItemNotFoundException;
import org.example.app.repository.NoteRepository;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.annotation.HasRole;
import org.example.framework.server.context.SecurityContext;
import org.example.framework.multipart.Part;
import org.example.framework.multipart.FormFieldPart;
import org.example.framework.multipart.FilePart;
import java.util.Map;
import java.util.HashMap;
import java.security.Principal;

import java.util.List;

import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Component
public class DefaultNoteManager implements NoteManager {
    private final NoteRepository repoNote;
    private final UserRepository repoUser;

    @HasRole("ROLE_USER")
    @Override
    public NoteRS create(Map<String, List<Part>> parts) {
        final Principal principal = SecurityContext.getPrincipal();
        Map<String, byte[]> image = getImage(parts);
        String content = getContent(parts);
        String author = principal.getName();
        String filename = null;
        byte[] mediaContent = null;
        if(!image.isEmpty()){
            for(String name : image.keySet()){
                filename = name;
            }
           mediaContent = image.get(filename);
        }
        UserEntity user = repoUser.getByLogin(author);
        NoteEntity note = new NoteEntity(0, content, mediaContent, user, false);
        repoNote.save(note);
        return new NoteRS(note.getContent(), note.getMedia(), note.isRemoved());
    }

    public Map<String, byte[]> getImage(Map<String, List<Part>> parts){
        Map<String, byte[]> image = new HashMap<>();
        for (List<Part> list: parts.values()){
            for (Part part:list){
                if(part instanceof FilePart){
                    image.put(((FilePart) part).getFileName(), ((FilePart) part).getContent());
                }
            }
        }
        return image;
    }
    public String getContent(Map<String, List<Part>> parts){
        String NoteContent = null;
        for (List<Part> list: parts.values()){
            for (Part part:list){
                if (part instanceof FormFieldPart){
                    if (part.getName().equals("content")){
                        NoteContent = ((FormFieldPart) part).getValue();
                    }

                }
            }
        }
        return NoteContent;
    }


    @HasRole("ROLE_USER")
    @Override
    public NoteRS getById(long id) {
        final Principal principal = SecurityContext.getPrincipal();
        final Optional<NoteEntity> note;
        note = repoNote.getById(id);
        String name = note.map(o -> o.getAuthor()
                        .getLogin())
                .orElseThrow(ItemNotFoundException::new);
        boolean removed = note.map(o -> o.isRemoved())
                .orElseThrow(ItemNotFoundException::new);
        if (removed == true) {
            throw new ItemNotFoundException("This note was deleted");
        }
        if (name.equals(principal.getName())) {
            return note
                    .map(o -> new NoteRS(o.getContent(), o.getMedia(), o.isRemoved()))
                    .orElseThrow(ItemNotFoundException::new)
                    ;
        } else {
            throw new ItemNotFoundException("You are not author");
        }
    }

    @HasRole("ROLE_USER")
    @Override
    public List<NoteRS> getAll() {
        final Principal principal = SecurityContext.getPrincipal();
        final List<NoteEntity> notes;
        notes = repoNote.getAll(0, 50);
        if (!notes.isEmpty()) {
            List<NoteRS> notesOfCurrentUser = notes.
                    stream().filter(note -> note.getAuthor().getLogin().equals(principal.getName()))
                    .map(o -> new NoteRS(o.getContent(), o.getMedia(), o.isRemoved()))
                    .collect(Collectors.toList());
            return notesOfCurrentUser;
        } else {
            throw new ItemNotFoundException("There are no notes of this author!");
        }

    }

    @HasRole("ROLE_USER")
    @Override
    public NoteRS removeById(long id) {
        final Principal principal = SecurityContext.getPrincipal();
        final Optional<NoteEntity> note;
        note = repoNote.getById(id);
        String name = note.map(o -> o.getAuthor()
                        .getLogin())
                .orElseThrow(ItemNotFoundException::new);
        boolean removed = note.map(o -> o.isRemoved())
                .orElseThrow(ItemNotFoundException::new);
        if (removed == true) {
            throw new ItemNotFoundException("This note was deleted");
        }
        if (name.equals(principal.getName())) {
            final NoteEntity noteEntity = note.get();
            noteEntity.setRemoved(true);
            repoNote.save(noteEntity);
            return new NoteRS(noteEntity.getContent(), noteEntity.getMedia(), noteEntity.isRemoved());
        } else {
            throw new ItemNotFoundException("You are not author");
        }
    }

    @HasRole("ROLE_USER")
    @Override
    public NoteRS updateById(long id, Map<String, List<Part>> parts) {
        final Principal principal = SecurityContext.getPrincipal();
        Map<String, byte[]> image = getImage(parts);
        String content = getContent(parts);
        String author = principal.getName();
        String filename = null;
        byte[] mediaContent = null;
        final Optional<NoteEntity> note;
        note = repoNote.getById(id);
        String name = note.map(o -> o.getAuthor()
                        .getLogin())
                .orElseThrow(ItemNotFoundException::new);
        boolean removed = note.map(o -> o.isRemoved())
                .orElseThrow(ItemNotFoundException::new);
        if (removed == true) {
            throw new ItemNotFoundException("This note was deleted");
        }
        if (name.equals(principal.getName())) {
            final NoteEntity noteEntity = note.get();
            noteEntity.setContent(content);
            if(!image.isEmpty()){
                for(String file : image.keySet()){
                    filename = file;
                }
                mediaContent = image.get(filename);
            }
            noteEntity.setMedia(mediaContent);
            repoNote.save(noteEntity);
            return new NoteRS(noteEntity.getContent(), noteEntity.getMedia(), noteEntity.isRemoved());
        } else {
            throw new ItemNotFoundException("You are not author");
        }
    }

}

