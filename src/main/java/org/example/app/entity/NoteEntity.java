package org.example.app.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "notes")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class NoteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String content;
    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] media;
    @ManyToOne
    @JoinColumn(name = "use_id")
    private UserEntity author;
    @Column(columnDefinition = "boolean default false")
    private boolean removed;
}
