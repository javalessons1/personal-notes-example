package org.example.app.repository;


import org.example.app.entity.NoteEntity;
import org.example.repository.CrudRepository;

public interface NoteRepository extends CrudRepository<NoteEntity, Long> {
}

