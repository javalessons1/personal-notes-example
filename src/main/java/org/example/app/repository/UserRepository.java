package org.example.app.repository;

import org.example.app.entity.UserEntity;
import org.example.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserEntity, Long> {
}

