package org.example.app;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.example.app.repository.NoteRepository;
import org.example.app.repository.UserRepository;
import org.example.framework.di.Container;
import org.example.framework.security.middleware.anon.AnonAuthMiddleware;
import org.example.framework.security.middleware.jsonbody.JSONBodyAuthNMiddleware;
import org.example.framework.security.processor.AuditBeanPostProcessor;
import org.example.framework.security.processor.HasRoleBeanPostProcessor;
import org.example.framework.server.annotation.Controller;
import org.example.framework.server.controller.ControllerRegistrar;
import org.example.framework.server.controller.method.converter.GsonHttpMessageConverter;
import org.example.framework.server.controller.method.handler.ReturnValueHandler;
import org.example.framework.server.controller.method.resolver.ArgumentResolver;
import org.example.framework.server.http.Server;
import org.example.repository.RepositoryCreator;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.example.app.manager.DefaultUserManager;

@Slf4j
public class Main {
    public static void main(String[] args) {
        // FIXME: don't hardcode password
        System.setProperty("javax.net.ssl.keyStore", "web-certs/server.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", "passphrase");

        final Container container = new Container();
        container.register("org.example");
        container.register(Gson.class);
        container.register(new Argon2PasswordEncoder());
        container.register(HasRoleBeanPostProcessor.class);
        container.register(AuditBeanPostProcessor.class);
        container.register(GsonHttpMessageConverter.class);
        final RepositoryCreator creator = new RepositoryCreator("default");
        creator.scan("org.example.app");
        final UserRepository userRepository = creator.provideBean(UserRepository.class);
        final NoteRepository noteRepository = creator.provideBean(NoteRepository.class);

        container.register(userRepository);
        container.register(noteRepository);
        container.wire();

        final Gson gson = container.getBean(Gson.class);
        final DefaultUserManager userManager = container.getBean(DefaultUserManager.class);

        final Server server = Server.builder()
                .middleware(new JSONBodyAuthNMiddleware(gson, userManager))
                .middleware(new AnonAuthMiddleware())
                .argumentResolvers(container.getBeansByType(ArgumentResolver.class))
                .returnValueHandlers(container.getBeansByType(ReturnValueHandler.class))
                .router(new ControllerRegistrar().register(container.getBeansByAnnotation(Controller.class)))
                .build();
        int port = 8443;
        try {
            server.start(port);
        } catch (Exception e) {
            log.debug("Error in main function occurred!");
        }
    }
}
